import React from 'react';
import { Route, Switch } from 'react-router-dom';

import HomePage from './pages/HomePage';
import LuisterenPage from './pages/LuisterenPage';
import VoorwaardenPage from './pages/VoorwaardenPage';
import OverPage from './pages/OverPage';
import StartPage from './pages/StartPage';

class Routes extends React.Component {
  render() {
    return (
      <Switch>
        <Route exact path='/' component={HomePage} />
        <Route exact path='/luisteren' component={LuisterenPage} />
        <Route exact path='/voorwaarden' component={VoorwaardenPage} />
        <Route exact path='/over-nuon' component={OverPage} />
        <Route exact path='/start' component={StartPage} />
        <Route render = { function() {
          return <h1>Not Found</h1>;
        }} />
      </Switch>
    );
  }
}

export default Routes;
