import React from 'react';

class VoorwaardenPage extends React.Component {
  render() {
    return (
        <div>
            <h1>Voorwaarden</h1>
            <a className="btn btn-start" href="/" target="_self">Start</a>
        </div>
    );
  }
}

export default VoorwaardenPage;