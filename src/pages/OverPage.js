import React from 'react';

class OverPage extends React.Component {
  render() {
    return (
        <div className="card card-image" id="over-nuon">
            <div className="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4">
                <div>
                    <h5 className="myblue-text"><i className="fa fa-microphone purple-text pr-2" aria-hidden="true"></i> Wij waarderen onze klanten</h5>
                    <h3 className="card-title pt-2"><strong>Over Nuon</strong></h3>
                    <p className="p-pages">We vragen er niet om, maar we hebben het allemaal nodig. Een schouderklopje. Een oprecht dank-je-wel voor al die jaren. Door te luisteren begrijp je hoe je iemand écht kunt waarderen. Luisteren geeft energie.</p>
                    <a className="btn btn-purple sub-btn" href="https://www.nuon.nl/" target="_blank" rel="noopener noreferrer">Kijk wat dit voor u betekent <i className="fa fa-chevron-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    );
  }
}

export default OverPage;
