import React, { Component } from "react";
import ReactDOM from 'react-dom';
import Bg1 from './../img/background/bg-1.jpg';
import Bg2 from './../img/background/bg-2.jpg';
import Bg3 from './../img/background/bg-3.jpg';
import Canvas from 'react-canvas-wrapper';

class ImageCanvas extends React.Component {
    componentDidMount() {
        this.refreshCanvas();
    }

    refreshCanvas = () => {
        const canvas = ReactDOM.findDOMNode(this.canvas);
        console.log(canvas);
        const ctx = canvas.getContext("2d");
        const image = this.refs.image;

        image.onload = () => {
            ctx.drawImage(image, 0, 0);
            ctx.font = "60px BebasNeue Bold";
        
            var x = 150;
            var y = 100;

            ctx.save();
            ctx.translate(x, y);
            ctx.rotate(-Math.PI / 75);
            ctx.textAlign = 'center';
            
            ctx.fillText(this.props.imageData.lightboard_line_1, 0, 8);
            ctx.fillText(this.props.imageData.lightboard_line_2, 0, 67);
            ctx.fillText(this.props.imageData.lightboard_line_3, 0, 125);
        };

        console.log(image);
        const dataURL = canvas.toDataURL()
        return dataURL;
    }
     
    render() {
        var imgStyle = { display:"none", fontFamily: "Nuon Matthew" };
        var bg_image = ''; 
        if (this.props) {
            if (this.props.imageData.bg_img === 3) {
                bg_image = Bg3;
            } else if (this.props.imageData.bg_img === 2) {
                bg_image = Bg2;
            } else {
                bg_image = Bg1;
            }
        }
        
        const pixelRatio = window.devicePixelRatio || 1;
        console.log(pixelRatio); // 1.0909090909090908
        const cWidth = 300 * pixelRatio; //image is default 300px
        const hWeight = 300 * pixelRatio; //image is default 300px

        return (
            <div id="canvas-preview">
                <Canvas canvasRef={(element) => { this.canvas = element; }} width={cWidth} height={hWeight} />
                <img ref="image" src={bg_image} alt="" style={imgStyle} />
            </div>
        );
    }
}

export default ImageCanvas;