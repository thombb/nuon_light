import React from 'react';
import { NavLink } from 'mdbreact';
import logo from './../logo-light.svg';

class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapseID: ''
    };
  }

  closeCollapse = collapseID => () => this.state.collapseID === collapseID && this.setState({ collapseID: '' });

  render(){
    return(
      <div>
        <a href="/" target="_self">
          <img src={logo} className="App-logo" alt="Nuon licht bericht" />
        </a>
        <h1>Schrijf een bericht voor een dierbare</h1>
        <p>en win een lightbox</p>
        <NavLink className="btn btn-start" onClick={this.closeCollapse('mainNavbarCollapse')} to="/start">
          Start
        </NavLink>
      </div>
    );
  }
}

export default HomePage;
