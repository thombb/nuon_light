import React from 'react';

class LuisterenPage extends React.Component {
  render() {
    return (
        <div className="card card-image" id="luisteren-geeft-energie">
            <div className="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4">
                <div>
                    <h5 className="myblue-text"><i className="fa fa-microphone purple-text pr-2" aria-hidden="true"></i> Wij waarderen onze klanten</h5>
                    <h3 className="card-title pt-2"><strong>Luisteren geeft energie</strong></h3>
                    <p className="p-pages">Energieleverancier van 2 miljoen mensen word je niet zomaar. Daar heb je veel ervaring voor nodig. We luisteren graag om te begrijpen wat u van ons nodig heeft. Zo bieden we graag iets extra’s. Want we waarderen iedere klant die bij ons blijft.</p>
                    <a className="btn btn-purple sub-btn" href="https://www.nuon.nl/luisteren/" target="_blank" rel="noopener noreferrer">Direct naar Nuon <i className="fa fa-chevron-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    );
  }
}

export default LuisterenPage;