import React from 'react';
import logo from './../logo-light.svg';
import result from './../img/background/test-result.jpg';
import ImageCanvas from './ImageCanvas';
import { Row, Col, Button } from 'mdbreact';

class StartPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
          formActivePanel1: 1,
          formActivePanel1Changed: false,
          bg_img: "1",
          lightboard_line_1: '',
          lightboard_line_2: '',
          lightboard_line_3: '',
          result: ''
        }

        this.onClick1 = this.onClick1.bind(this);
        this.onClick2 = this.onClick2.bind(this);
        this.onClick3 = this.onClick3.bind(this);
    }

    swapFormActive = (a) => (param) => (e) => {
        this.setState({
          ['formActivePanel' + a]: param,
          ['formActivePanel' + a + 'Changed']: true
        });
    }
    
    handleNextPrevClick = (a) => (param) => (e) => {
        this.setState({
          ['formActivePanel' + a] : param,
          ['formActivePanel' + a + 'Changed']: true
        });
    }

    handleShareClick = (a) => {
        //get a and redirect
        console.log(a);
    }
    
    handleSubmission = () => {
        console.log('finished');
    }

    calculateAutofocus = (a) => {
        if (this.state['formActivePanel'+a+'Changed']) {
          return true
        }
    }

    onClick1() {
        this.setState({bg_img: 1});
    }

    onClick2() {
        this.setState({bg_img: 2});
    }

    onClick3() {
        this.setState({bg_img: 3});
    }

    submitHandler = (event) => {
        event.preventDefault();
        //check if form element has was-validated className
        console.log(event.target.className);
        if (!event.target.className.match(/was-validated/g)) {
            event.target.className += ' was-validated';
        }

        if (this.state.lightboard_line_1.length === 0) {
            var errorDiv = document.getElementById('show-default-error');
            errorDiv.style.display = (errorDiv.style.display  ? 'block' : 'none' );
        } else {
            var img_src = "./../img/background/bg-"+this.state.bg_img+".jpg";

            //set state
            this.setState({
                'formActivePanel1' : 3,
                'formActivePanel1Changed': true,
                result: img_src
            });

            //handle submit
            this.handleSubmission();
        }
    }
    
    changeHandler = (event) => {
        if (event.target.name === 'lightboard_line_1' && event.target.value !== '') {
            var errorDiv = document.getElementById('show-default-error');
            errorDiv.style.display = (errorDiv.style.display ? 'none' : 'block' );
        }
        //set new state
        this.setState({...this.state, [event.target.name]: event.target.value})
    }
    
    render() {

        const styleErrorDisplay = { display: "none"};
        
        return (
            <div>
                <div>
                    <a href="/" target="_self">
                        <img src={logo} className="App-logo smaller" alt="Nuon licht bericht" />
                    </a>
                </div>
                <form action="post" className="needs-validation" noValidate id="form-nuon-light" onSubmit={this.submitHandler}>
                    <Row>
                    { this.state.formActivePanel1 === 1  &&
                    (<Col md="12" className="image-selector">
                        <p className="p-pages">Stap 1</p>
                        <h1 className="form-step">Kies een foto</h1>
                        <input onClick={this.onClick1} defaultChecked={this.state.bg_img === 1 ? true : false} id="img1" type="radio" name="bg_img" value="1" onChange={this.changeHandler} required/>
                        <label className="image-cc img1" htmlFor="img1"></label>
                        <input onClick={this.onClick2} defaultChecked={this.state.bg_img === 2 ? true : false} id="img2" type="radio" name="bg_img" value="2" onChange={this.changeHandler} required />
                        <label className="image-cc img2" htmlFor="img2"></label>
                        <input onClick={this.onClick3} defaultChecked={this.state.bg_img === 3 ? true : false} id="img3" type="radio" name="bg_img" value="3" onChange={this.changeHandler} required />
                        <label className="image-cc img3" htmlFor="img3"></label>
                        <Button className="btn form-next-btn" onClick={this.handleNextPrevClick(1)(2)}>Volgende</Button>
                    </Col>)}

                    { this.state.formActivePanel1 === 2  &&
                    (<Col md="12">
                        <p className="p-pages">Stap 2</p>
                        <h1 className="form-step">Schrijf hier jouw boodschap</h1>
  
                        <div className="invalid-feedback alert" id="show-default-error" style={styleErrorDisplay}>
                            Schrijf hier eerst jou persoonlijke boodschap
                        </div>

                        <input placeholder="type hier" maxLength="10" data-target="1" className="form-control lightboard-line lightboard-line-1" name="lightboard_line_1" value={this.state.lightboard_line_1} onChange={this.changeHandler} type="text" required />
                        { this.state.lightboard_line_1.length ? (
                            <div className="counter counter-1">
                                <span>{ 10 - this.state.lightboard_line_1.length }</span>/10
                            </div>
                        ) : null }
                        <input placeholder="jouw" maxLength="10" data-target="2" className="form-control lightboard-line lightboard-line-2" name="lightboard_line_2" value={this.state.lightboard_line_2} onChange={this.changeHandler} type="text" />
                        { this.state.lightboard_line_2.length ? (
                            <div className="counter counter-2">
                                <span>{ 10 - this.state.lightboard_line_2.length }</span>/10
                            </div>
                        ) : null }
                        <input placeholder="bericht" maxLength="10" data-target="3" className="form-control lightboard-line lightboard-line-3" name="lightboard_line_3" value={this.state.lightboard_line_3} onChange={this.changeHandler} type="text" />
                        { this.state.lightboard_line_3.length ? (
                            <div className="counter counter-3">
                                <span>{ 10 - this.state.lightboard_line_3.length }</span>/10
                            </div>
                        ) : null }
                        <Button className="btn form-next-btn" type="submit">Volgende</Button>
                    </Col>)}

                    { this.state.formActivePanel1 === 3  &&
                    (<Col md="12">
                        <p className="p-pages">Stap 3</p>
                        <h1 className="form-step">Deel jou bericht</h1>
                        
                        <div id="result">
                           <ImageCanvas imageData={this.state} defaultResult={result} />
                        </div>

                        <div className="share-btns">
                            <Button className="btn-floating btn-lg purple" onClick={this.handleShareClick('fb')} title="Share on facebook"><i className="fa fa-facebook" aria-hidden="true"></i></Button>
                            <Button className="btn-floating btn-lg purple" onClick={this.handleShareClick('tw')} title="Share on twitter"><i className="fa fa-twitter" aria-hidden="true"></i></Button>
                            <Button className="btn-floating btn-lg purple" onClick={this.handleShareClick('dl')} title="Download"><i className="fa fa-download" aria-hidden="true"></i></Button>
                        </div>
                    </Col>)}
                    </Row>
                </form>
            </div>
        );
    }
}

export default StartPage;