import React, { Component } from 'react';
import { Navbar, NavbarNav, NavbarToggler, Collapse, NavItem, NavLink } from 'mdbreact';
import { BrowserRouter as Router } from 'react-router-dom';
import logoNuon from './logo-nuon.png';
import './App.css';
import './index.css';

import Routes from './Routes';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapseID: ''
    };
  }

  toggleCollapse = collapseID => () => {
    this.setState(prevState => ({ collapseID: (prevState.collapseID !== collapseID ? collapseID : '') }));
  }

  closeCollapse = collapseID => () => this.state.collapseID === collapseID && this.setState({ collapseID: '' });

  render() {
    const overlay = <div id="sidenav-overlay" style={{backgroundColor: 'transparent'}} onClick={this.toggleCollapse('mainNavbarCollapse')} />;
    return (
      <Router>
        <div className="App">
          { this.state.collapseID && overlay}
          <main className="App-container container text-center">
            <Routes />
          </main>
          <footer id="footer">
            <div className="container footer-container">
              <a href="/" target="_self" className="logo-nuon float-right">
                <img src={logoNuon} alt="logo Nuon" />
              </a>
              <Navbar sticky="bottom">
                <NavbarToggler onClick={this.toggleCollapse('mainNavbarCollapse')} title="Menu">
                  <i className="fa fa-bars" aria-hidden="true"></i>
                </NavbarToggler>
                <Collapse id='mainNavbarCollapse' isOpen={this.state.collapseID} navbar>
                  <div>
                    <button onClick={this.closeCollapse('mainNavbarCollapse')} className="navbar-close">
                      <i className="fa fa-close" aria-hidden="true"></i> close
                    </button>
                  </div>
                  <NavbarNav>
                    <NavItem>
                        <NavLink onClick={this.closeCollapse('mainNavbarCollapse')} to="/">
                          Home
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink onClick={this.closeCollapse('mainNavbarCollapse')} to="/luisteren">
                          Luisteren geeft energie
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <a href="http://lichtbericht.nl/pdf/actievoorwaarden.pdf" target="_blank" rel="noopener noreferrer" onClick={this.closeCollapse('mainNavbarCollapse')}>
                          Voorwaarden
                        </a>
                    </NavItem>
                    <NavItem>
                        <NavLink onClick={this.closeCollapse('mainNavbarCollapse')} to="/over-nuon">
                          Over Nuon
                        </NavLink>
                    </NavItem>
                  </NavbarNav>
                </Collapse>
              </Navbar>
            </div>
          </footer>
        </div>
      </Router>
    );
  }
}

export default App;